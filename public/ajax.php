<?php
    //configuration
    require("../includes/config.php");


    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $username = $_POST["username"];
        $sector = $_POST["sector"];

        //checking in db that if default_settings is on or off for this user
        $sql = $dbh->prepare("SELECT * FROM settings WHERE user = ?");
        $sql->execute([$username]);
        $row = $sql->fetch(PDO::FETCH_ASSOC);

        if(empty($row["default_settings"]))
        {

            //then send the sector link form this current row
            $link = $row[$sector];

            /*links in db are stored as https://youtu.be/something but embed links
             are like https://www.youtube.com/embed/something, converting the
             saved links to embeded links */
            $position = strripos($link, "/");
            $substring = substr($link, $position + 1);
            $newlink = "https://www.youtube.com/embed/{$substring}";

            /*FOR LOCKING MECHANISM*/
            $sql = $dbh->prepare("UPDATE settings SET locked = ? , lock_link = ? WHERE user = ?");
            $sql->execute(["on", $newlink, $row["user"]]);

            $data = ["link" => $newlink];

            //sending header
            header("Content-type: application/json");
            print(json_encode($data));
        }
        else
        {
            //that means default settings is on
            //get default setting
            $sql = $dbh->prepare("SELECT * FROM default_settings");
            $sql->execute();
            $row = $sql->fetch(PDO::FETCH_ASSOC);

            $link = $row[$sector];

            /*links in db are stored as https://youtu.be/something but embed links
             are like https://www.youtube.com/embed/something, converting the
             saved links to embeded links */
            $position = strripos($link, "/");
            $substring = substr($link, $position + 1);
            $newlink = "https://www.youtube.com/embed/{$substring}";

            /*FOR LOCKING MECHANISM*/
            $sql = $dbh->prepare("UPDATE settings SET locked = ? , lock_link = ? WHERE user = ?");
            $sql->execute(["on", $newlink, $username]);


            $data = ["link" => $newlink];

            //sending header
            header("Content-type: application/json");
            print(json_encode($data));
        }
    }
?>
