$(document).ready(function()
{
    $("div").on("dblclick", function(e)
    {
        //getting the iframe video tag
        var video = document.getElementsByTagName('iframe')[0];

        //getting the location url and getting username from url
        var url_string= window.location.href;
        var url_object = new URL(url_string);
        var username = url_object.searchParams.get("user");


        //getting the target where the click was made and getting that divs id
        var target = e.target;
        var id = target.id;

        //using switch based on id of clicked div
        switch (id)
        {
            case "sector1":
                getlink(username, id);
                break;
            case "sector2":
                getlink(username, id);
                break;
            case "sector3":
                getlink(username, id);
                break;
            case "sector4":
                getlink(username, id);
                break;
            case "sector5":
                getlink(username, id);
                break;
            case "sector6":
                getlink(username, id);
                break;
            case "sector7":
                getlink(username, id);
                break;
            case "sector8":
                getlink(username, id);
                break;
        }
    });


});


/*Function to make ajax calls requires username and sector name*/
function getlink(username, sectorname)
{
    $.ajax({
        type: "POST",
        url: "ajax.php",
        data:{"username": username, "sector" : sectorname},
        success : function(data)
                {

                    //changing the src of iframe
                    $("#video > iframe").attr("src", data.link);
                    //console.log(data);

                    //remove event handlers
                    $("div").off();
                },
        fail : function()
                {
                    console.log("error");
                }

    });
}
