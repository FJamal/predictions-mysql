<?php
    //configuration
    require("../includes/config.php");

    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $username = $_POST["username"];

        //checking if username exists
        $sql = $dbh->prepare("SELECT * FROM settings WHERE user = ?");
        $sql->execute([$username]);
        $row = $sql->fetch(PDO::FETCH_ASSOC);

        if(empty($row))
        {
            // username not in db
            $data = ["username" => false ];
            header("Content-type:application/json");
            print(json_encode($data));
        }
        else
        {
            //if username already in db
            $data = ["username" => true];
            header("Content-type:application/json");
            print(json_encode($data));
        }

    }
?>
