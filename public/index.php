<?php
    require("../includes/config.php");

    if($_SERVER["REQUEST_METHOD"] == "GET")
    {
        //if trying to visit prediction page via user get variable
        if(isset($_GET["user"]))
        {
            //initializing this to avoid error in helpers.php in getusername() function
            $_SESSION["id"]="";

            //check if this user is there and his app is activated
            $sql = $dbh->prepare("SELECT * FROM settings WHERE user =?");
            $sql->execute([$_GET["user"]]);
            $row = $sql->fetch(PDO::FETCH_ASSOC);

            //if user doesnot exist row would be empty
            if(empty($row))
            {
                render("error.php", ["message" => "This page does not exist", "title" => "Error"]);
            }
            else
            {
                /* LOGICS TO DISPLAY DIFFERENT GET VIEWS BASED ON SETTINGS */

                //get full name of user from users table
                $sql2 = $dbh->prepare("SELECT * FROM users WHERE username =?");
                $sql2->execute([$row["user"]]);
                $row2 = $sql2->fetch(PDO::FETCH_ASSOC);
                $name = $row2["fullname"];

                //print_r($row);
                if ($row["Activated"] == "yes" && $row["default_settings"] == "yes" && $row["trainer"] == "on")
                {
                    /*Trainers on with default settings*/
                    //get the default settings
                    $sql = $dbh->prepare("SELECT * FROM default_settings");
                    $sql->execute();
                    $settings = $sql->fetch(PDO::FETCH_ASSOC);

                    //link to send
                    $link = $settings["sector1"];

                    //converting link to embeded form
                    $newlink = convertlink($link);

                    render("get_view.php", ["title" => "{$row["user"]}'s Prediction'",
                                            "link" => $newlink,
                                            "trainer" => true,
                                            "profilepic" => $row["profile_image"],
                                            "name" => $name]);

                }
                else if($row["Activated"] == "yes" && empty($row["default_settings"]) && $row["trainer"] == "on")
                {
                    /*Trainers ON WITH SAVED SETTINGS*/
                    //settings would already be there in $row
                    $link = convertlink($row["sector1"]);

                    render("get_view.php", ["title" => "{$row["user"]}'s Prediction",
                                            "link" => $link,
                                            "trainer" => true,
                                            "profilepic" => $row["profile_image"],
                                            "name" => $name]);

                }

                else if($row["Activated"] == "yes" && $row["default_settings"] == "yes"
                        && empty($row["trainer"]))
                {
                    /*DEAFAULT SETTINGS ON NO TRAINERS*/
                    //now possible views based on if app is locked or not
                    if(empty($row["locked"]))
                    {
                        /*lock is not "on" display sector1 link with script in get_view.php*/

                        //get the default settings
                        $sql = $dbh->prepare("SELECT * FROM default_settings");
                        $sql->execute();
                        $settings = $sql->fetch(PDO::FETCH_ASSOC);

                        $link = convertlink($settings["sector1"]);

                        render("get_view.php", ["title" => "{$row["user"]}'s Prediction",
                                                "link" => $link,
                                                "profilepic" => $row["profile_image"],
                                                "name" => $name]);
                    }
                    else
                    {
                        /*lock is "on" load the link form lock_link column from settings table*/

                        //link saved in $row and set $lock in get_view.php so that script is not loaded
                        render("get_view.php", ["title" => "{$row["user"]}'s Prediction",
                                                "link" => $row["lock_link"],
                                                "lock" => true,
                                                "profilepic" => $row["profile_image"],
                                                "name" => $name]);
                    }

                }
                else if($row["Activated"] == "yes" && empty($row["default_settings"])
                        && empty($row["trainer"]))
                {
                    /*SAVED SETTINGS ON NO TRAINERS */
                    //now possible views based on if app is locked or not
                    if(empty($row["locked"]))
                    {
                        /*lock is off display with sector1 from settings thats stored in $row
                        and include the script in get_view.php by not including a lock variable*/
                        $link = convertlink($row["sector1"]);

                        render("get_view.php", ["title" => "{$row["user"]}'s Prediction",
                                                "link" =>$link,
                                                "profilepic" => $row["profile_image"],
                                                "name" => $name]);
                    }
                    else
                    {
                        //lock is off set link to saved locked link from column lock_link in settings table
                        //and add a lock variable to remove the script
                        render("get_view.php", ["title" => "{$row["user"]}'s Prediction",
                                                "link" => $row["lock_link"],
                                                "lock" => true,
                                                "profilepic" => $row["profile_image"],
                                                "name" => $name]);
                    }

                }
                else if(empty($row["Activated"] ))
                {
                    render("error.php", ["message" => "This page does not exist", "title" => "Error"]);
                }
            }
        }
        else
        {
            /*USER VISITING ACTUALLY HOMEPAGE, IT RENDERS HOMEPAGE VIEW WITH ALL THE DATA*/
            $username = getusername();
            //getting users saved configuration
            $sql = $dbh->prepare("SELECT * FROM settings WHERE user =?");
            $sql->execute([$username]);
            $row = $sql->fetch(PDO::FETCH_ASSOC);

            //get full name from users table
            $sql = $dbh->prepare("SELECT *FROM users WHERE username =?");
            $sql->execute([$username]);
            $row2 = $sql->fetch(PDO::FETCH_ASSOC);
            //print_r($row2);

            render("homepage-view.php", ["title" => "Home Page", "data" => $row, "fullname" => $row2["fullname"]]);
        }
    }
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {

        // if acttivate/deactivate not clicked then save was clicked
        if (!isset($_POST["activate"]) && !isset($_POST["deactivate"]))
        {
            //get the key from $_POST so that it can be used to store in db
            $sector = key($_POST);
            //insert into db
            try
            {
                //getting user name
                $data = $dbh->query("SELECT username FROM users WHERE user_id = {$_SESSION["id"]}");
                $username = $data->fetch();
                $username = $username[0];

                /*sqlite doesnot support on duplicate key so first checking if a row
                with the username already exist in db*/
                $sql = $dbh->prepare("SELECT * FROM settings WHERE user = ?");
                $sql->execute([$username]);
                $row = $sql->fetch(PDO::FETCH_ASSOC);
                if (empty($row))
                {
                    //that means row with username doesnot exsist so add username too in db and sector value
                    $sql = $dbh->prepare("INSERT INTO settings (user, {$sector}) VALUES (?,?)");
                    $sql->execute([$username , $_POST[$sector]]);
                }
                else
                {
                    /*just add the sector link update is used to keep the change in that same
                    row otherwise new rows were created */
                    $sql = $dbh->prepare("UPDATE settings SET {$sector} = ? WHERE user = ?");
                    $sql->execute([$_POST[$sector], $username]);
                }

                //reloading index.
                redirect("index.php");

            }
            catch (PDOException $e)
            {
                print("Could not save item" . $e->getMessage());
            }
        }
        else
        //if activate/deactivate button was clicked
        {
            $username = getusername();
            if(isset($_POST["activate"]))
            {

                /*sqlite doesnot support on duplicate key so first checking if a row
                with the username already exist in db*/
                $sql = $dbh->prepare("SELECT * FROM settings WHERE user = ?");
                $sql->execute([$username]);
                $row = $sql->fetch(PDO::FETCH_ASSOC);
                if (empty($row) && isset($_POST["defaultsettings"]) && isset($_POST["trainer"]))
                {
                    //that means row with username doesnot exsist so add username too in db set activated plus default settings flag in db
                    $sql = $dbh->prepare("INSERT INTO settings (user, Activated, default_settings, trainer) VALUES (?,?,?,?)");
                    $sql->execute([$username , "yes", "yes", "on"]);
                    redirect("index.php");
                }
                else if(empty($row) && !isset($_POST["defaultsettings"]) && !isset($_POST["trainer"]))
                {
                    //activating for the first time without default settings
                    $sql = $dbh->prepare("INSERT INTO settings (user, Activated, trainer) VALUES (?,?,?)");
                    $sql->execute([$username, "yes", ""]);
                    redirect("index.php");
                }
                else if(!empty($row) && !isset($_POST["defaultsettings"]) && !isset($_POST["trainer"]))
                {
                    /*just set the activated flag and default_settings flag, using update row otherwise new rows were created */
                    $sql = $dbh->prepare("UPDATE settings SET Activated = ?, default_settings = ?, trainer = ? WHERE user = ?");
                    $sql->execute(["yes","","", $username]);
                    redirect("index.php");
                }
                else if(!empty($row) && isset($_POST["defaultsettings"]) && isset($_POST["trainer"]))
                {
                    $sql = $dbh->prepare("UPDATE settings SET Activated = ?, default_settings = ?, trainer =? WHERE user = ?");
                    $sql->execute(["yes","yes","on", $username]);
                    redirect("index.php");
                }
                /*start for trainer code */
                else if (!empty($row) && (isset($_POST["defaultsettings"]) || isset($_POST["trainer"])))
                {
                    if(isset($_POST["defaultsettings"]))
                    {
                        $sql = $dbh->prepare("UPDATE settings SET Activated = ?, default_settings = ?, trainer =? WHERE user = ?");
                        $sql->execute(["yes","yes","", $username]);
                        redirect("index.php");
                    }
                    else
                    {
                        $sql = $dbh->prepare("UPDATE settings SET Activated = ?, default_settings = ?, trainer =? WHERE user = ?");
                        $sql->execute(["yes","","on", $username]);
                        redirect("index.php");
                    }
                }
                else if (empty($row) && (isset($_POST["defaultsettings"]) || isset($_POST["trainer"])))
                {
                    if(isset($_POST["defaultsettings"]))
                    {
                        //that means row with username doesnot exsist so add username too in db set activated plus default settings flag in db and trainer settings
                        $sql = $dbh->prepare("INSERT INTO settings (user, Activated, default_settings, trainer) VALUES (?,?,?,?)");
                        $sql->execute([$username , "yes", "yes", ""]);
                        redirect("index.php");
                    }
                    else
                    {
                        $sql = $dbh->prepare("INSERT INTO settings (user, Activated, default_settings, trainer) VALUES (?,?,?,?)");
                        $sql->execute([$username , "yes", "", "on"]);
                        redirect("index.php");
                    }
                }
            }

            //if deactivate button was clicked
            if(isset($_POST["deactivate"]))
            {
                //changing activated from "yes" to "" and removing the lock and lock_link.
                $sql = $dbh->prepare("UPDATE settings SET Activated = ?, locked = ?, lock_link = ? WHERE user = ?");
                $sql->execute(["","", "", $username]);
                redirect("index.php");
            }
        }
    }
?>
