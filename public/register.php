<?php
    //configuration
    require("../includes/config.php");

    if($_SERVER["REQUEST_METHOD"] == "GET")
    {
        //render the register form
        render("register-view.php", ["title" => "Register"]);
    }
    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $name = $_POST["name"];
        $email = $_POST["email"];
        $username = $_POST["username"];
        $password = $_POST["password"];
        $confirmation = $_POST["confirmation"];
        $pin = $_POST["pinpassword"];
        //any field is empty
        if(empty($name) || empty($email) || empty($username) || empty($password)
            || empty($confirmation) || empty($pin))
        {
            //render the register form
            render("register-view.php", ["title" => "Register", "servervalidation" => "Please fill all the fields"]);
        }
        else
        {
            if(!empty($email) && strpos($email, "@") == false)
            {
                render("register-view.php", ["title" => "Register",
                                            "servervalidation" => "Invalid email id"]);
            }
            else if($password != $confirmation)
            {
                render("register-view.php", ["title" => "Register",
                        "servervalidation" => "Passwords dont match"]);
            }
            else if ($password == $confirmation)
            {
                //check if user name in db or not
                $sql = $dbh->prepare("SELECT * FROM settings WHERE user = ?");
                $sql->execute([$username]);
                $row = $sql->fetch(PDO::FETCH_ASSOC);

                if(empty($row))
                {
                    /* username not in db*/
                    $password = password_hash($password, PASSWORD_DEFAULT);

                    //check is pinpassword is a valid one
                    $sql3 = $dbh->prepare("SELECT * FROM passwords WHERE password =?");
                    $sql3->execute([$pin]);
                    $row3 = $sql3->fetch(PDO::FETCH_ASSOC);
                    if(empty($row3))
                    {
                        /*not a vali password*/
                        render("register-view.php", ["title" => "Register",
                                "servervalidation" => "Invalid Pin Password"]);
                    }
                    else
                    {
                        /*password is valid but check if its not already used*/
                        if($row3["used"] == "no")
                        {
                            //not a used password
                            $sql = $dbh->prepare("INSERT INTO users (username, password, email, fullname) VALUES (?, ?,?,?)");
                            $sql->execute([$username, $password, $email, $name]);

                            //create row in settings table in db
                            $sql = $dbh->prepare("INSERT INTO settings (user) VALUES (?)");
                            $sql->execute([$username]);

                            //getting user id
                            $sql = $dbh->prepare("SELECT * FROM users WHERE username = ?");
                            $sql->execute([$username]);
                            $row = $sql->fetch(PDO::FETCH_ASSOC);

                            $_SESSION["id"] = $row["user_id"];

                            //update the password tables and save the pin password as used one
                            $sql = $dbh->prepare("UPDATE passwords SET used = ?, used_by =?, email=? WHERE password = ?");
                            $sql->execute(["yes", $username, $email, $pin]);

                            redirect("index.php");
                        }
                        else
                        {
                            // a used password
                            render("register-view.php", ["title" => "Register",
                                    "servervalidation" => "The Pin Password has already been used"]);
                        }
                    }



                }
                else
                {
                    //if username already in db
                    render("register-view.php", ["title" => "Register",
                            "servervalidation" => "Username already in use, please select another"]);
                }
            }
            else
            {
                /*Invalid admin password */
                render("register-view.php", ["title" => "Register",
                        "servervalidation" => "Invalid admin password"]);
            }
        }
    }
?>
