<?php
/*function to load the viewpage and its variables */
function render($view, $values = [])
{
    if($view == "login_view.php")
    {
        extract($values);
        require("../views/{$view}");
    }
    else if($view != "login.php" && file_exists("../views/{$view}"))
    {
        //extract the values passed to this page
        extract($values);
        require("../views/header.php");
        require("../views/{$view}");
        require("../views/footer.php");
    }
    else
    {
        trigger_error("invalid view = {$view}", E_USER_ERROR);
    }
}

function redirect($page)
{
    /* Redirect to a different page in the current directory that was requested */
    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $extra = $page;
    header("Location: http://$host$uri/$extra");
    exit;
}


//Get username from db
function getusername()
{
    //need to use $dbh but since its in global scope so using it from $GLOBALS
    //getting user name
    /*function only works when session id is not empty cuz when visited via
    get variable this function was giving notice so when the website is visited via
    get variable $_SESSION["id"] is set to empty string to avoid this error*/
    if(!empty($_SESSION["id"]))
    {
        $data = $GLOBALS["dbh"]->query("SELECT username FROM users WHERE user_id = {$_SESSION["id"]}");
        $username = $data->fetch();
        $username = $username[0];
        return $username;
    }
}

//Converts the saved links to links that can be embeded in iframes
function convertlink($link)
{
    //converting link to embeded form
    $position = strripos($link, "/");
    $substring = substr($link, $position + 1);
    $newlink = "https://www.youtube.com/embed/{$substring}";
    return $newlink;
}



function logout()
{
	//unset the sessions
	$_SESSION = [];

	//expires the cookie
	if(!empty(session_name()))
	{
		setcookie(session_name(), "", time() - 36000);
	}

	//destroy sessions
	session_destroy();
}


/* loads username and all the appropriate data to be displayed in homepage-view.php
    together with error message from displaypic.php takes in a string as error message
    for argument */
function display_profilepic_error($message)
{
    $username = getusername();
    //getting users saved configuration
    $sql = $GLOBALS["dbh"]->prepare("SELECT * FROM settings WHERE user =?");
    $sql->execute([$username]);
    $row = $sql->fetch(PDO::FETCH_ASSOC);

    //bug fix get the name of user as well
    $sql2 =  $GLOBALS["dbh"]->prepare("SELECT * FROM users WHERE username = ?");
    $sql2->execute([$username]);
    $row2 = $sql2->fetch(PDO::FETCH_ASSOC);



    render("homepage-view.php", ["title" => "Home Page", "data" => $row,
                                "uploadflag" => $message,
                                "fullname" => $row2["fullname"]]);
}

?>
