<?php
    session_start();

    ini_set("display_errors", true);
    error_reporting(E_ALL);

    $dsn = "mysql:host=localhost;dbname=predictions";


    //connecting to db
    try
    {
        $dbh = new PDO($dsn, "root", "baadshah");
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
        print("Could not connect to data base" . $e->getMessage());
    }

    //including all the helper functions
    require("helpers.php");

    if(!isset($_GET["user"]) && !isset($_GET["id"]) && !in_array($_SERVER["PHP_SELF"], ["/login.php", "/ajax.php", "/register.php" , "/ajax-username.php"]))
    {
        if(empty($_SESSION["id"]))
        {
            redirect("login.php");
        }
    }
    //need to code if user visits using get variables >> coded in index.php.

?>
