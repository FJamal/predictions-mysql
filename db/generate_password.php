<?php
    //connecting to database
    $dsn = "mysql:host=localhost;dbname=predictions";


    //connecting to db
    try
    {
        $dbh = new PDO($dsn, "root", "baadshah");
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
        print("Could not connect to data base" . $e->getMessage());
    }

    if($argc < 2)
    {
        print("Invalid argument. Usage:filename password length");
        exit(1);
    }

    else if($argc == 2)
    {
        $length = (int)$argv[1];
        if(is_int($length))
        {
            $possiblechars = "0123456789abcdefghijklmnopqrstuvwxyz";

            $inarray = str_split($possiblechars);
            while(true)
            {
                //empty string for password
                $password = "";
                for ($i = 0; $i < $length; $i++)
                {
                    //more useful would be random_int() in php 7
                    $key = rand(0, count($inarray) - 1);
                    $password = $password . $inarray[$key];
                }
                //checking if password already exist in db
                $sql = $dbh->prepare("SELECT * FROM passwords WHERE password = ?");
                $sql->execute([$password]);
                $row = $sql->fetch(PDO::FETCH_ASSOC);

                if(empty($row))
                {
                    //password doesnt exist, enter password and break from while loop
                    $sql = $dbh->prepare("INSERT INTO passwords (password, used) VALUES (?,?)");
                    $sql->execute([$password, "no"]);
                    break;
                }
                else
                {
                    continue;
                }
            }

            print("password:{$password}");

        }
    }
?>
