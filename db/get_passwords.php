<?php
    //connecting to db
    $dsn = "mysql:host=localhost;dbname=predictions";


    //connecting to db
    try
    {
        $dbh = new PDO($dsn, "root", "baadshah");
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
        print("Could not connect to data base" . $e->getMessage());
    }

    //getting all the passwords
    $rows = $dbh->query("SELECT * FROM passwords WHERE used = 'no'");

    $filehandle  = fopen("password.txt", "w");
    foreach($rows as $row)
    {
        print($row["password"]. "\n");
        fwrite($filehandle, "{$row["password"]}" .PHP_EOL);
    }
    fclose($filehandle);

?>
