<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div id = "userpanel">
                <?php if(empty($data["profile_image"])):?>
                    <img id = "displaypic" src="/images/profile-icon.png"  class="img-thumbnail img-responsive">
                <?php elseif(!empty($data["profile_image"]) && file_exists("./uploads/{$data["profile_image"]}")): ?>
                    <img id = "displaypic" src="/uploads/<?= $data["profile_image"] ?>"  class="img-thumbnail img-responsive">

                <?php else: ?>
                    <img id = "displaypic" src="/images/profile-icon.png"  class="img-thumbnail img-responsive">
                <?php endif; ?>
                <div id="userinfo">
                    <h3><?= $fullname ?></h3>
                    <h5>username:<?= $data["user"]?></h5>
                    <form class="form-group" action="index.php" method="post">

                        <!--for activate deactivate button functionality -->
                        <?php if(!empty($data["Activated"])): ?>
                            <div class="form-group">
                                <input type="submit" name="activate" value="Activate" class = "btn btn-primary" disabled="disabled">
                                <input type="submit" name="deactivate" value="Deactivate" class = "btn btn-primary" >
                            </div>
                        <?php else: ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name = "defaultsettings">Default settings
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name = "trainer">Trainers
                                </label>
                            </div>
                            <div class="form-group">
                                <input type="submit" name="activate" value="Activate" class = "btn btn-primary" >
                                <input type="submit" name="deactivate" value="Deactivate" class = "btn btn-primary" disabled="disabled" >
                            </div>
                        <?php endif; ?>

                    </form>


                </div>
                <form enctype="multipart/form-data" action="displaypic.php" method="post">
                    <div class="form-group" id = "Dp">
                        <input type="hidden" name="MAX_FILE_SIZE" value="200000">
                        <input  type="file" name = "picture">
                        <!-- adding file upload error msgs from profilepic.php -->
                        <?php if(isset($uploadflag)): ?>
                            <p><?= $uploadflag ?></p>
                        <?php endif; ?>
                        <input type="submit" class = "btn btn-success" name="profilepic" value="Upload Pic">
                        <input type="submit" name="logout" value="LogOut" class = "btn btn-success">
                    </div>

                </form>
                <!-- logout button -->


            </div>
        </div>

    </div>

    <!--start for section to store links of videos -->
    <div id = "settings">
        <h1>Settings</h1>
        <?php for($i = 0; $i < 7 ; $i += 2): ?>
            <!-- needed to do below step to fill the settings form with saved value -->
            <?php $counter = $i; $counter += 1;?>
        <div class="row">
            <div class="col-xs-6">
                <form class="form-inline" action="index.php" method="post">
                    <div class="form-group">
                        <label for="sector 1">Sector <?= $i + 1 ?></label>
                        <input type="text" class = "form-control" name="sector<?= $i + 1 ?>"  placeholder="Enter Video Link" value = "<?= $data["sector"."{$counter}"];?>">

                    </div>
                    <input type="submit" value="Save" class="btn btn-success">
                </form>
            </div>
            <!-- needed to do below step to fill the settings form with saved value -->
            <?php $counter = $i; $counter += 2;?>
                <div class="col-xs-6">
                    <form class="form-inline" action="index.php" method="post">
                        <div class="form-group">
                            <label for="sector 1">Sector <?= $i +2 ?></label>
                            <input type="text" class = "form-control" name="sector<?= $i + 2 ?>" placeholder="Enter Video Link" value = "<?= $data["sector"."{$counter}"]?>">

                        </div>
                        <input type="submit" value="Save" class="btn btn-success">
                    </form>
                </div>
        </div>
            <?php endfor; ?>
    </div>

    <!--START OF TUTORIAL SECTION -->
    <div class="row">
        <div class="col-xs-12">
            <h1>How It Works?</h1>
            <p>This web app allows you to do different prediction tricks
            that could have eight possible outs. The app comes with a preloaded
            settings of videos that could be launched through the app's launching
            panel at the top by checking the default settings check box and activating
            the app or you could save your own predictions as unlisted youtube videos and save their links in the above settings panel. Once the app is launched the performer can show his/her
            prediction by using his/her username in the url address inside of chrome
            or any other browser. My intentions was to have this app located at
            something like <b>www.mypredictions.com</b> then the performer could
            show his/her prediction by asking the spectator to visist <br> www.mypredictions.com?user=<b>YOURUSERNAME</b>
            But since this is a prototype this apps url would be different from www.mypredictions.com.
        </p>
        <p>The current url for this would be something like <?= $_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"] ?></p>
        <p>Set your app to default settings and trainers "on", from the top and activate the app
        Now in the address bar of your browser the address would be something like <?= $_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"] ?>
        . In this link delete the <b>index.php</b> part and insert <b>?user=YOUR USERNAME</b>.Hit go
        observe what you see on that page and then come back to this page.</p>
        <p>The page you visited was the page where you would show the spectator your prediction video.
        The different colors you saw were due to the trainers that you set to on before launching the app.
        The trainers as the name suggest is only for your own practice. You never turn them on when you are actually
        performing. That page is divided into 8 sectors as shown by different colors </p>

        </div>
    </div>
    <div class="row">
        <div id = "s1" class="col-xs-1">
            <p>sector1</p>
        </div>
        <div id = "s2" class="col-xs-1">
            <p>sector2</p>
        </div>
        <div id = "s3" class="col-xs-1">
            <p>sector3</p>
        </div>
    </div>

    <div class="row">
        <div id = "s4" class="col-xs-1">
            <p>sector4</p>
        </div>
        <div id = "svid" class="col-xs-1">
            <p>Video</p>
        </div>
        <div id = "s5" class="col-xs-1">
            <p>sector5</p>
        </div>
    </div>
    <div class="row">
        <div id = "s6" class="col-xs-1">
            <p>sector6</p>
        </div>
        <div id = "s7" class="col-xs-1">
            <p>sector7</p>
        </div>
        <div id = "s8" class="col-xs-1">
            <p>sector8</p>
        </div>

    </div>
    <div class="row">
        <div class="col-xs-12">
            <p>All of these areas on the screen when the prediction page is loaded
            are sensitive to double tabs on the screen of your or the spectator's device. When
            an area belonging to certain sector is double clicked the appropriate youtube video
            saved for that sector is loaded without having to put anything in the adress bar or doing anything else
            . The default settings have videos saved that shows a single playing card turned face up
            in the entire face down deck. The default configuration relative to sectors placement on screen is </p>
            <p>|7 of clubs||8 of clubs||9 of clubs| <br>
             |10 of clubs||VIDEO||J of clubs| <br>
            |Q of clubs||K of clubs||A of clubs| <br> </p>
            <br><br>
            <p>The other secret is the locking mechanism of this app. Once you
            double click/tab on a particular sector to load a desired video the app locks itself
            on that video and the double click functionality stops working and even if
            the spectator refresh the page from the browser the app would be locked on that video with no double click functionality
            until you deactivate the app from its launching panel and activate it again. This locking functionality is not turned on when the trainers
            are on from the apps launching panel so that you could practice and not have to deactivate and activate the app again and again.
            When the trainers are on the locking mechanism is off but you will have to refresh the page after every double tap in order to
            keep the double tap functionality.</p>
            <h3>A Simple Trick</h3>
            <p>Activate the app with trainers off and default settings on.You start by asking the spectator that have they ever watched any prediction
            trick on tv or in person where the magician asks to use instagram or youtube and bam there on
            the magician's page there is a prediction of some event that just happened. Now wait for their answer
            Then tell them that personally im not a fan of such tricks i mean I always think what if those ppl don't have
            instagram or youtube on their cell phones.Then asks them to take out their mobile device and ask them to go on chrome
            emphasizing that atleast everyone has chrome on their phones. Ask them to visit the link this app
            They will see a login page.Tell them this is a website where magicians make predictions and post them here. But you cant
            see anyone's post until you are logged in, or have that users profile link, much like facebook and twitter.
            Take out a piece of paper and write address of your page, it should be <b>?user=yourusername</b>, fold the paper and hand them the paper saying
            this is my profile link where i made a certain prediction. Remember I gave you this even before any thing happened. Then you begin a simple trick
            imagine I have a deck of 52 cards, there are high cards and low cards, which one would you like, then asks them about the color red or black,
            then the suit. Basically you will have to force high cards of clubs via Equivocation to the spectator. When the suit is selected take their attention
            back to the piece of paper that you wrote and ask them to visit that link.(You can guide them as where to start from the "?" mark for the link).
            When the page is loaded just take the phone momentarily on the pretext of showing them that there is just a single video on this page with your name photo and date on which it was posted(date would always be the date of the previous day).
            You can even scroll up and down on their device to show them the page. While talking for a moment turn the screen of their phone towards you. Ask them
            if they want to change their card? Depending on their answer double tap on the appropriate sector and the video will load within a fraction of second and hand them back theri phone saying I dont even wanna touch it and you yourself play the video
            . And it will be their card, from here the app would be locked and they could refresh the page as much as they want or even try double tapping on the screen and nothing would change. If you were doing the trick for a large
            audience you can ask everyone to go andcheck the video themselves.(<b>After when the app is locked</b>)</p>
        </br></br>
        <h3>A few Things to Remember</h3>
        <ul>
            <li>The prediction page will be online for as long as the app is activated.</li>
            <li>Though there is a slight possibility that you showed this trick to someone he remembered your profile
                link and after a few days you activated your app to show the trick to someone else and at the same time the person
                you showed this trick decides to visit your profile again and he locks the app and you would have no idea that the app got locked. So
                it is advisible to activate it just a few moments before the performance.</li>
            <li>Only youtube links you get via pressing the share button on a youtube video works with this app. Those links are like <b>https://youtu.be/xyz</b>.</li>
            <li>Some audience management is required so practice with the trainers on before actually performing it</li>
            <li>While practicing you will notice that the video section on screen flickers for a fraction of a second to change the video.If you are using custom prediction videos
            it is advisable to have a blank black thumbnail for those videos and a "." for their title to minimize the chances of anyone noticing that the video was changed </li>
            <li>It is advisable to upload a display pic to make the predictions more authentic. The app at the moment allows png, jpeg and jpg of 200kb or less</li>
        </ul>

        </div>

    </div>




</div>
