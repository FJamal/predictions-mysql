<?php
    date_default_timezone_set("Asia/Karachi");
    $yesterday = date("F j  , Y ", strtotime("-1 day"));
 ?>
<div class="container-fluid">
    <div class="row">
        <!--<div id = "top">
            condition to display profile pic
            <?php if(empty($profilepic)): ?>
                <img src="/images/title.jpeg" class="img-rounded img-responsive" alt="Responsive image">
            <?php else: ?>
                <img src="/uploads/<?= $profilepic ?>" class="img-rounded img-responsive" alt="Responsive image">
            <?php endif; ?>
        </div> -->
        <div class="col-xs-4"><h3><?= $name ?></h3></div>
        <div class="col-xs-4">
            <?php if(empty($profilepic)): ?>
                <img src="/images/profile-icon.png" class="img-rounded img-responsive" alt="Responsive image">
            <?php elseif(!empty($profilepic) && file_exists("./uploads/{$profilepic}")): ?>
                <img src="/uploads/<?= $profilepic ?>" class="img-rounded img-responsive" alt="Responsive image">
            <?php else: ?>
                <img src="/images/profile-icon.png" class="img-rounded img-responsive" alt="Responsive image">
            <?php endif; ?>
        </div>
        <div class="col-xs-4">
            <p> posted on </p>
            <p id ="date"><?= $yesterday ?></p>
        </div>
    </div>
    <div class="row">
        <div id = "sector1" class="col-xs-4"></div>
        <div id = "sector2" class="col-xs-4"></div>
        <div id = "sector3" class="col-xs-4"></div>
    </div>
    <div class="row">
        <div id = "sector4" class="col-xs-2"></div>
        <div id = "video" class="col-xs-8">
            <iframe width="100%" height="100%" src="<?= $link ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
        <div id = "sector5" class="col-xs-2"></div>
    </div>
    <div class="row">
        <div id = "sector6" class="col-xs-4"></div>
        <div id = "sector7" class="col-xs-4"></div>
        <div id = "sector8" class="col-xs-4"></div>
    </div>
</div>

<!-- for locking mechanism -->
<?php if(!isset($lock)): ?>
    <script src="../js/loader.js"></script>
<?php endif; ?>
